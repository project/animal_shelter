(function ($) {
  Drupal.behaviors.convertDropdownToUL = {
    attach: function (context) {
       //converting select box into ul li
if (!$('#views-exposed-form-event-block-1 .form-item-field-event-trend-taxonomy ul' ).length) {
  var $Filter = $(".event-on-trend #views-exposed-form-event-block-1 .form-select ");
  $Filter.find("option").map(function () {
    var $this = $(this);
    return $("<li>").attr("value", $this.attr("value")).text($this.text()).get();
  }).appendTo($("<ul data-drupal-selector='edit-field-event-trend-taxonomy' name='field_event_trend_taxonomy' class='filter-form-list'>").attr({
    id: $Filter.attr("id"),
  })).parent().once().appendTo("#views-exposed-form-event-block-1 .form-item-field-event-trend-taxonomy-target-id");

}
$(".filter-form-list li").click(function (){
  var selectedValue = $(this).attr("value");
    $(".form-select").val(selectedValue).change();
  $('.form-submit').trigger('click');
})

    }
  };
})(jQuery);

// owl carousel code
jQuery(document).ready(function () {
  jQuery('.events-slider').owlCarousel({
      loop: false,
      autoplay: true,
      dots: false,
      autoplaySpeed: 1000,
      autoplayTimeout: 2000,
      autoplayHoverPause: true,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut',
      stopOnHover: true,
      nav: true,
      responsive: {
          0: {
              items: 1
          },
          991: {
              items: 2
          },
          767: {
              items: 2
          },
          1024: {
              items: 3
          }
      }
  });

// Scroll To Top

jQuery(window).scroll(function () {
  if (jQuery(this).scrollTop() > 100) {
    jQuery('.scrolltop').fadeIn('slow');
  } else {
    jQuery('.scrolltop').fadeOut('slow');
  }
});
jQuery('.scrolltop').click(function () {
  jQuery('html, body').animate( { scrollTop: 0 }, 'slow');
});
  });

// hamburger code
burger = document.querySelector('.hamburger');
bar = document.querySelectorAll('.bar');
navbar = document.querySelector('.nav-menu');
burger.addEventListener('click', () => {
  if(navbar.classList.contains('nav-menu')){
     burger.classList.add('displace');
     navbar.classList.remove('nav-menu')
     navbar.classList.add('nav-items-1')
     bar.forEach(function (e) {
      return e.classList.add("rotate");
    });
  }
  else{
     burger.classList.remove('displace');
     navbar.classList.add('nav-menu')
     navbar.classList.remove('nav-items-1')
     bar.forEach(function (e) {
      return e.classList.remove("rotate");
    });
     }
  });
